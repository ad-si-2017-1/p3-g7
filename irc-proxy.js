var irc = require('irc');
var amqp = require('amqplib/callback_api');


var proxies = {}; // mapa de proxys
var amqp_conn;
var amqp_ch;
var irc_client;

// Conexão com o servidor AMQP
amqp.connect('amqp://localhost', function(err, conn) {
	conn.createChannel(function(err, ch) {
		amqp_conn = conn;
		amqp_ch = ch;

		inicializar();
	});
});

function inicializar () {

	receberDoCliente("registro_conexao", function (msg) {

		var id       = msg.id;
		var servidor = msg.servidor;
		var nick     = msg.nick;
		var canal    = msg.canal;

		irc_client = new irc.Client(
			servidor, 
			nick,
			{channels: [canal]}
		);

		irc_client.addListener('message'+canal, function (from, message) {
		    
		    console.log(from + ' => '+ canal +': ' + message);
		    
		    enviarParaCliente(id, {
                "type": "msg",
		    	"timestamp": Date.now(), 
				"nick": from,
				"msg": message
			});

		});

        irc_client.addListener('registered', function(message) {
		    enviarParaCliente(id, {
                "type": "reg",
		    	"timestamp": Date.now(), 
				"msg": 'Conectado em: irc://'+nick+'+@'+servidor+'/'+canal
			});
        });

		irc_client.addListener('error', function(message) {
		    console.log('error: ', message);
		    enviarParaCliente(id, {
                "type": "msg",
                "timestamp": Date.now(), 
                "nick": "IRC Server",
                "msg": "<font color='red'>" + message.command + "</font>"

			}); 
		});

		irc_client.addListener('names', function(channel, nicks){
		    enviarParaCliente(id, {
                "type": "names",
                "timestamp": Date.now(), 
                "msg": JSON.stringify(nicks)

			}); 
        irc_client.addListener('+mode', function(channel,by,mode,argument,message) {
		    enviarParaCliente(id, {
                "type": "msg",
                "timestamp": Date.now(), 
                "nick": "IRC Server",
                "msg": '<font color = "Green">' + by + ' alterou o mode de ' + argument +' para: ' + mode + '</font>'
			}); 
        });
	});
	    /*
	      recebe comandos IRC de forma mais "crua", verifica que comando é
	      e envia o JSON apropiado para o servidor RabbitMQ
	    */
        irc_client.addListener('raw', function(message) {
          switch(message.command){
             case "PRIVMSG":
 		       enviarParaCliente(id, {
                "type": "msg",
                "timestamp": Date.now(), 
                "nick": message.nick,
                "msg": message.args[1]

			   });               
               break;
             case "PART":
                enviarParaCliente(id, {
                  "type":"part",
                  "timestamp": Date.now(), 
                  "nick": message.nick
               });
               break;
            case "JOIN":
                enviarParaCliente(id, {
                  "type":"join",
                  "timestamp": Date.now(), 
                  "nick": message.nick
               });              
              break;
          }
        });
       


		proxies[id] = irc_client;
	});
    /*
      recebe mensagem do servidor RabbitMQ, verifica o tipo da mensagem, se 
      é um comando para o servidor ou uma mensagem entre usuários.
    */
	receberDoCliente("gravar_mensagem", function (msg) {
		console.log(msg);
        switch(msg.type){
          case "msg":
		    irc_client.say(msg.canal, msg.msg);
            break;
          case "cmd":
            irc_client.send(msg.msg);
            break;
        }
	});
}





/*
  consulta servidor RabttiMQ e recebe mensagens pendentes no canal <canal> passado no paramêtro,
  retornando para uma função anonima em <callback> a mensagem em formato JSON
*/
function receberDoCliente (canal, callback) {

	amqp_ch.assertQueue(canal, {durable: false});

    console.log(" [irc] Waiting for messages in ", canal);
    
    amqp_ch.consume(canal, function(msg) {

      console.log(" [irc] Received %s", msg.content.toString());
      callback(JSON.parse(msg.content.toString()));

    }, {noAck: true});
}

/*
  envia mensagem em msg para o canal RabbitMQ.
  O canal é uma string com user_+ <id> 
*/
function enviarParaCliente (id, msg) {

	msg = new Buffer(JSON.stringify(msg));

	amqp_ch.assertQueue("user_"+id, {durable: false});
	amqp_ch.sendToQueue("user_"+id, msg);
	console.log(" [irc] Sent %s", msg);
}




