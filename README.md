## PROJETO  - APLICAÇÕES DISTRIBUÍDAS DO GRUPO 7

MEMBROS
> Eduardo Lapa

> Bruna Carneiro

> Israel Alves

> Jessica Milene

## Objetivo
O objetivo do projeto 3 é desenvolver um sistema de troca de mensagens multiprotocolo, conforme o esboço da arquitetura, com as seguintes características/funcionalidades:

> Uso do protocolo AMQP para troca de mensagens entre produtores e consumidores;

>  Uso de uma interface Web para comunicação multi-chat, para envio e recebimento de mensgens AMQP;

> Os canais de comunicação serão expresso através de URLs, por exemplo:

irc://fulano@irc.freenode.net:6667/#ad-si-2017-1 (protocolo IR, conectado no servidor irc.freenode.net, na port 6667, no canal #ad-si-2017-1 como fulano

tg://fulano:123@ad-si-2017-1?auth=xyz (protocolo Telegram, com usuário fulano utilizando senha '123', no canal ad-si-2017-1 com autenticação 'xyz' (hash id de convite de grupo)

> Será obrigatório desenvolver um proxy AMQP/IRC, que faz a comunicação entre o servidor de fila de mensagens (MQ Server) e o servidor IRC.

> Será opcional (com bônus de 20% cada) de um proxy AMQP/Telegram e/ou cliente de notificações em plataforma android que receba notificações de mensagens novas não lidas.

![Arquitetura do projeto](https://ead.inf.ufg.br/pluginfile.php/89624/mod_forum/intro/index.png)