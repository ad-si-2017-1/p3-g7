/*
  Adiciona <mensagem> no <elemento_id>. 
*/
function adiciona_mensagem(mensagem,elemento_id,timestamp) {
	var novo_elemento = document.createElement('div');
	novo_elemento.id = "mensagem"+timestamp;
	document.getElementById(elemento_id).appendChild(novo_elemento);
	document.getElementById('mensagem'+timestamp).innerHTML=mensagem;
}

/*
  Transforma timestamp em formato HH:MM:SS
*/
function timestamp_to_date( timestamp ) {
	var date = new Date( timestamp );
	var hours = date.getHours();
	var s_hours = hours < 10 ? "0"+hours : ""+hours;
	var minutes = date.getMinutes();
	var s_minutes = minutes < 10 ? "0"+minutes : ""+minutes;
	var seconds = date.getSeconds();
	var s_seconds = seconds < 10 ? "0"+seconds : ""+seconds;
	return s_hours + ":" + s_minutes + ":" + s_seconds;
}

function iniciar(elemento_id) {
	$("#status").text("Conectando em - irc://"+
			Cookies.get("nick")+"@"+
			Cookies.get("servidor")+"/"+
			Cookies.get("canal") + "...");
	carrega_mensagens(elemento_id,0);
}

function addUsuariosSelecao(usuarios){

    var novo_elemento = document.createElement('option');
    novo_elemento.text = Cookies.get("canal");
    document.getElementById('usuarios').add(novo_elemento); 
  for (var key in usuarios){
    var novo_elemento = document.createElement('option');
    novo_elemento.text = key;
    document.getElementById('usuarios').add(novo_elemento); 
  }
}

function add_usuario(usuario, timestamp){
  adiciona_mensagem('<font color = "green">' + usuario  + ' entrou na sala.</font>', 'mural', timestamp);
  var repetido = false;
  var select = document.getElementById('usuarios');
  for(var i = 0; i<select.length; i++){
    if (select.options[i].value == usuario){
      repetido = true;
    }
  }
  if(!repetido){
    var novo_elemento = document.createElement('option');
    novo_elemento.text = usuario;
    document.getElementById('usuarios').add(novo_elemento); 
  }
}

function remove_usuario(usuario, timestamp){
  adiciona_mensagem('<font color = "red">' + usuario  + ' saiu da sala.</font>', 'mural', timestamp);
  var select = document.getElementById('usuarios');
  for(var i = 0; i<select.length; i++){
    if (select.options[i].value == usuario){
      select.remove(i);
    }
  }
}
/*
  Carrega as mensagens ocorridas após o <timestamp>,
  acrescentando-as no <elemento_id>
*/
var novo_timestamp="0";
var names = true;
function carrega_mensagens(elemento_id, timestamp) {
	var mensagem = "";
	var horario = "";    
	$.get("obter_mensagem/"+timestamp, function(data,status) {
		if ( status == "success" ) {
		    var linhas = data;
		    for ( var i = linhas.length-1; i >= 0; i-- ) {
                switch(linhas[i].type){
                  case "msg":
                  		    	horario = timestamp_to_date(linhas[i].timestamp);
			mensagem = 
				"["+horario+" - "+
				linhas[i].nick+"]: "+
		                linhas[i].msg;
			novo_timestamp = linhas[i].timestamp;
		    	    adiciona_mensagem(mensagem,elemento_id,novo_timestamp);
                    break;
                 case "reg":
                   $("#status").text(linhas[i].msg);
                   break;
                 case "names":
                   if(names == true){
                     var usuarios = JSON.parse(linhas[i].msg);
                     addUsuariosSelecao(usuarios);
                     novo_timestamp = linhas[i].timestamp;
                     names = false;
                   }
                   break;
                 case "part":
                   novo_timestamp = linhas[i].timestamp;
                   remove_usuario(linhas[i].nick, novo_timestamp);
                   break;
                 case "join":
                   novo_timestamp = linhas[i].timestamp;
                   add_usuario(linhas[i].nick, novo_timestamp);
                   break;
                }
			}
		}
		else {
		    alert("erro: "+status);
		}
		}
	);
	t = setTimeout( 
		function() { 
			carrega_mensagens(elemento_id,novo_timestamp) 
		}, 
		1000);		
}

/*
   Submete a mensagem dos valores contidos s elementos identificados 
   como <elem_id_nome> e <elem_id_mensagem>
*/
function submete_mensagem(elem_id_mensagem) {
	var mensagem= document.getElementById(elem_id_mensagem).value;
    var msg = '';
    if (mensagem.lastIndexOf("/", 0) === 0){
	   msg = '{"type":"cmd", "timestamp":'+Date.now()+','+
		  '"nick":"'+Cookies.get("nick")+'",'+
                  '"msg":"'+mensagem.split("/")[1]+'", "canal":"' + document.getElementById('usuarios').value+ '"}'; 
    }
    else{
	   msg = '{"type":"msg", "timestamp":'+Date.now()+','+
		  '"nick":"'+Cookies.get("nick")+'",'+
                  '"msg":"'+mensagem+'", "canal":"' + document.getElementById('usuarios').value+ '"}';
   }
   document.getElementById(elem_id_mensagem).value = "";
	$.ajax({
		type: "post",
		url: "/gravar_mensagem",
		data: msg,
		success: 
		function(data,status) {
			if (status == "success") {
			    // nada por enquanto
			}
			else {
				alert("erro:"+status);
			}
		},
		contentType: "application/json",
		dataType: "json"
		});
       
}

function trocarMode(elemento){

	var usuario = Cookies.get("nick");
	var args = $("#"+elemento).val();
	var comando = "mode/"+usuario+"/"+args;
$.get(comando, function(data,status) {
		if ( status == "success" ) {
		    
		alert(comando);
}
		});
}
